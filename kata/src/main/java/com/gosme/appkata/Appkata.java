package com.gosme.appkata;
import com.gosme.roman.RomanBiz;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Appkata {
    static final Logger logger = LoggerFactory.getLogger(Appkata.class);
    public static void main(String[] params) {
        logger.info("hello {}", RomanBiz.romanToInt("MMXXII"));
    }
}
