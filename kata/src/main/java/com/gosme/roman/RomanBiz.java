package com.gosme.roman;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;
public class RomanBiz {
    private RomanBiz() {}
    static final Logger logger = LoggerFactory.getLogger(RomanBiz.class);
    private static Map<Character, Integer> romanNumber = Map.of(
            'I', 1, 'V', 5, 'X', 10, 'L', 50, 'C', 100, 'D', 500, 'M', 1000);
    public static int romanToInt(String roman) {
        if (!RomanRules.allRulesOk(roman)) return 0 ;
        int intVal = 0;
        for (int i = 0; i < roman.length(); i++) {
            int actualN = romanNumber.get(roman.charAt(i));
            if (i == roman.length() - 1) {
                intVal += actualN;
            } else {
                int nextN = romanNumber.get(roman.charAt(i + 1));
                if (actualN >= nextN) intVal += actualN;
                else intVal -= actualN;
            }
        }
        return intVal;
    }
}
