package com.gosme.roman;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RomanRules {
    RomanRules() {}
    static final Logger logger = LoggerFactory.getLogger(RomanRules.class);
    static boolean allRulesOk(String roman){
        return onlyRomanOk(roman) && max3Ok(roman) && noDoubleOk(roman) && noVXOk(roman) && notNullOk(roman);
    }
    static boolean notNullOk(String roman) {
        if (roman.isEmpty()){
            logger.error("erreur chaine vide");
            return false;
        }
        return true;
    }
    static boolean onlyRomanOk(String roman) {
        if (roman.matches("[IVXLCDM]+"))
            return true;
        logger.error("erreur character non romain : {}", roman);
        return false;
    }
    static boolean max3Ok(String roman) {
        if (roman.contains("IIII") || roman.contains("LLLL") || roman.contains("XXXX")) {
            logger.error("erreur + de 3 répetitions : {}", roman);
            return false;
        }
        return true;
    }
    static boolean noDoubleOk(String roman) {
        if (roman.contains("LL") || roman.contains("DD") || roman.contains("VV")) {
            logger.error("erreur + de 2 répetitions : {}", roman);
            return false;
        }
        return true;
    }
    static boolean noVXOk(String roman) {
        if (roman.contains("VX")) {
            logger.error("erreur VX  : {}", roman);
            return false;
        }
        return true;
    }
}
