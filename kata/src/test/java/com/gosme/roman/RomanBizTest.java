package com.gosme.roman;

import org.junit.jupiter.api.Test;

import static com.gosme.roman.RomanBiz.romanToInt;
import static org.junit.jupiter.api.Assertions.*;

class RomanBizTest {
    @Test
    void romamToIntTest() {
        assertEquals(476, romanToInt("CDLXXVI"));
        assertEquals(960, romanToInt("CMLX"));
        assertEquals(483, romanToInt("CDLXXXIII"));
        assertEquals(994, romanToInt("CMXCIV"));
        assertEquals(0, romanToInt("format error"));
    }
}
