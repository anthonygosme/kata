package com.gosme.roman;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static com.gosme.roman.RomanBiz.romanToInt;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RomanBizCucumber {
    String romain ;
    @When("le nombre romain={string}")
    public void le_nombre_romain(String string) {
        romain=string ;
    }
    @Then("la valeur retour={int}")
    public void la_valeur_retour(Integer int1) {
        assertEquals(int1, romanToInt(romain)) ;
    }
}
