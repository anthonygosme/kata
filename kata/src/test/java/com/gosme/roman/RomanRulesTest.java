package com.gosme.roman;
import org.junit.jupiter.api.Test;
import static com.gosme.roman.RomanRules.*;
import static org.junit.jupiter.api.Assertions.*;

class RomanRulesTest {

    @Test
    void onlyRomanTest() {
        assertTrue(onlyRomanOk("CDLXXVI"));
        assertFalse(onlyRomanOk("XXB"));
        assertFalse(onlyRomanOk(" XX"));
    }
    @Test
    void max3Test() {
        assertTrue(max3Ok("CDLXXVI"));
        assertTrue(max3Ok("XXXB"));
        assertFalse(max3Ok("LLLLX"));
    }
    @Test
    void noDoubleTest() {
        assertTrue(noDoubleOk("CDLXXVI"));
        assertFalse(noDoubleOk("DDI"));
    }
    @Test
    void noVXTest() {
        assertTrue(noVXOk("CDLXXVI"));
        assertFalse(noVXOk("VXI"));
    }

    @Test
    void checkAllRulesTest() {
        assertTrue(allRulesOk("CDLXXVI"));
        assertFalse(allRulesOk("DDIV"));
    }

    @Test
    void NotNullTest() {
        assertTrue(notNullOk("CDLXXVI"));
        assertFalse(notNullOk(""));
    }
}
